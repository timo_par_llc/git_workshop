#!/usr/bin/env ruby

require 'yaml'

$count = 0
def log(string)
    $count += 1
    timestamp = Time.now.strftime("%Y-%m-%d %H:%M:%S")
    puts "#{timestamp}: I: #{string}" if $count <= 50
end

def upload_fotos_to_facebook
    puts "Great now people can create AI clones of me and my family"
end

def load_config
    begin
        $config = YAML.load_file('config.yml')
    rescue Errno::ENOENT
        puts "Config file not found"
        exit
    end
    puts "Config loaded"
end

def parser_BFF_processor
    puts $config['proessors']['BFF']
end

puts "Hello guys I am starting"
105.times do |i|
    log "This is line #{i+1}"
    upload_fotos_to_facebook()
end

puts "I am done now. Go get a cold one."
